//
//  SingletonAFnet.h
//  SampleWeather
//
//  Created by koneti santhosh kumar on 23/11/17.
//  Copyright © 2017 iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

//Protocol
@protocol webserviceProtocol<NSObject>
-(void)didGetResult:(NSDictionary*)Dic;
@end


@interface SingletonAFnet : NSObject

@property(nonatomic,assign) id <webserviceProtocol>protocolDelegate;

//singleton Declare
+ (id)singletonSharedManager;

//instance method:declare webserver
-(void)getDataFromserver:(NSString*)urlStr;

@end
