//
//  SingletonAFnet.m
//  SampleWeather
//
//  Created by koneti santhosh kumar on 23/11/17.
//  Copyright © 2017 iOS. All rights reserved.
//

#import "SingletonAFnet.h"

@implementation SingletonAFnet


//Singleton : Instance
+ (id)singletonSharedManager{
    
    static SingletonAFnet   *singletonObject = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [[self alloc] init];
        
    });
    
    return singletonObject;
    
}

-(void)getDataFromserver:(NSString*)urlStr{
    
    NSString *str = urlStr;
    NSURL *url = [NSURL URLWithString:str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [[[NSURLSession sharedSession]dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(error == nil){
            NSDictionary *firstDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            if ([self.protocolDelegate respondsToSelector:@selector(didGetResult:)])
                [self.protocolDelegate didGetResult:firstDic];
        }
    }]resume];

}

@end
