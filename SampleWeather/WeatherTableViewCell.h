//
//  WeatherTableViewCell.h
//  SampleWeather
//
//  Created by koneti santhosh kumar on 24/11/17.
//  Copyright © 2017 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *weatherDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *wkLbl;
@property (weak, nonatomic) IBOutlet UILabel *lowForcastLbl;
@property (weak, nonatomic) IBOutlet UILabel *highForcastLbl;
@property (weak, nonatomic) IBOutlet UILabel *textLbl;

@end
