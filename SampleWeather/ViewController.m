//
//  ViewController.m
//  SampleWeather
//
//  Created by koneti santhosh kumar on 23/11/17.
//  Copyright © 2017 iOS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    dateArry = @[@"11 Jan 2017, Wed",@"12 Jan 2017, Thu",@"13 Jan 2017, Fri",@"14 Jan 2017, Sat",@"15 Jan 2017, Sun"];
//    weatherRange = @[@"79 - 84",@"79-83",@"78 - 82",@"78 - 83",@"76 - 82"];
    
    DateArray = [[NSMutableArray alloc]init];
    wkArray = [[NSMutableArray alloc]init];
    lowForcastArray = [[NSMutableArray alloc]init];
    highForcastArray = [[NSMutableArray alloc]init];
    textArray = [[NSMutableArray alloc]init];
    forcastArray = [[NSMutableArray alloc]init];

    //Navigation Bar Content
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};

    //Calling:protocol
    [[SingletonAFnet singletonSharedManager]getDataFromserver:@"https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22singapore%2C%20sg%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"];
   
    SingletonAFnet *instance = [SingletonAFnet singletonSharedManager];
    instance.protocolDelegate = self;
    
//    [self.weatherRptTblView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didGetResult:(NSDictionary *)Dic{
//    NSLog(@"Weather %@",Dic);
    NSDictionary *queryDic = [Dic valueForKey:@"query"];
//    NSLog(@"Result : %@",queryDic.allKeys);
    NSDictionary *resultDic = [queryDic valueForKey:@"results"];
//    NSLog(@"result Dic %@",resultDic.allKeys);
    NSDictionary *channelDic = [resultDic valueForKey:@"channel"];
//    NSLog(@"Channel : %@ ",channelDic.allKeys);
    NSDictionary *itemDic = [channelDic valueForKey:@"item"];
//    NSLog(@"item : %@",itemDic.allKeys);
    forcastArray = [itemDic valueForKey:@"forecast"];
    NSLog(@"forecast  : %@",forcastArray);
    
    NSDictionary *conditionDic = [itemDic valueForKey:@"condition"];
    self.dateLbl.text = [conditionDic valueForKey:@"date"];
    self.tempDip.text = [conditionDic valueForKey:@"temp"];
    self.TextLbl.text = [conditionDic valueForKey:@"text"];
    
    [_weatherTableview reloadData];
}

- (void)viewWillAppear:(BOOL)animated; {
     [_weatherTableview reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{

    return forcastArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
   WeatherTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
 
    NSDictionary *forcastDic = [forcastArray objectAtIndex:indexPath.row];
    cell.weatherDateLbl.text = [forcastDic valueForKey:@"date"];
    cell.wkLbl.text = [forcastDic valueForKey:@"day"];
    cell.lowForcastLbl.text = [forcastDic valueForKey:@"low"];
    cell.highForcastLbl.text = [forcastDic valueForKey:@"high"];
    cell.textLbl.text = [forcastDic valueForKey:@"text"];
    return cell;
}

@end
