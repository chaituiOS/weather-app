//
//  AppDelegate.h
//  SampleWeather
//
//  Created by koneti santhosh kumar on 23/11/17.
//  Copyright © 2017 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

