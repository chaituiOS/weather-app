//
//  ViewController.h
//  SampleWeather
//
//  Created by koneti santhosh kumar on 23/11/17.
//  Copyright © 2017 iOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SingletonAFnet.h"
#import "WeatherTableViewCell.h"

@interface ViewController : UIViewController<webserviceProtocol,UITableViewDelegate,UITableViewDataSource>

{
    NSMutableArray  *DateArray;
    NSMutableArray *wkArray;
    NSMutableArray *lowForcastArray;
    NSMutableArray *highForcastArray;
    NSMutableArray *textArray;
    NSMutableArray *forcastArray;
    
}


@property (weak, nonatomic) IBOutlet UITableView *weatherTableview;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UILabel *tempDip;

@property (weak, nonatomic) IBOutlet UILabel *TextLbl;


@end

